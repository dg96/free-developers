<style type="text/css">
	.websites{
		padding-top: 3em;
		padding-bottom: 3em;
		background-image: linear-gradient(rgba(0,0,0,0.7), rgba(0,0,0,0.7)), url(../img/jumbotron.jpg);
		background-attachment: fixed;
		background-size: cover;
	}
	.websites a{color: white;}
	.websites a:hover{color: white; text-decoration: none;}
	.websites a:focus{color: white; text-decoration: none;}
</style>
<div class="main-title" id="section-3">
	<h2> I nostri siti </h2>
</div>

<div class="websites">
	<div class="container">
		<div class="row">
			<div class="m-auto">
				<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
					<ol class="carousel-indicators">
						<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
						<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
						<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
					</ol>
					<div class="carousel-inner">
						<div class="carousel-item active">
							<img class="d-block w-100" src="img/WorldTecno.jpg" alt="First slide">
							<div class="carousel-caption d-none d-md-block">
								<h5>Raffaele Tufano</h5>
								<p><a href="https://www.worldtecno.com" target="_blank"> Vai al sito </a></p>
							</div>
						</div>
						<div class="carousel-item">
							<img class="d-block w-100" src="img/waku-seo.jpg" alt="Second slide">
							<div class="carousel-caption d-none d-md-block">
								<h5>Philip Waku</h5>
								<p> <a href="https://waku-seo.com/it/" target="_blank"> Vai al sito </a> </a> </p>
							</div>
						</div>
						<div class="carousel-item">
							<img class="d-block w-100" src="img/teck-developer.jpg" alt="Second slide">
							<div class="carousel-caption d-none d-md-block">
								<h5>Denis Genitoni</h5>
								<p> <a href="https://teck-developer.com" target="_blank"> Vai al sito </a> </a> </p>
							</div>
						</div>
					</div>
					<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Previous</span>
					</a>
					<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Next</span>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>