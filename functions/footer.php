<style type="text/css">
#footer {
    width: 100%;
    height: auto;
}

.footer-top {
    background-image: linear-gradient(to left bottom, #333333, #2f2f2f, #2a2a2a, #262626, #222222);
    color: #eee;
    width: 100%;
    height: auto;
    padding: 40px;
}

.social-icon i {
    margin: 5px;
}

.social-icon a {
    color: #eee;
}

.social-icon a:hover {
    color: #5D6D7E;
}

.footer-bottom {
    background-color: #5D6D7E;
    color: #eee;
    width: 100%;
    height: auto;
    padding: 20px;
}

#top-btn {
    display: inline-block;
    background-color: #2980B9;
    width: 50px;
    height: 50px;
    text-align: center;
    border-radius: 5px;
    margin: 30px;
    position: fixed;
    bottom: 30px;
    right: 30px;
    transition: background-color .3s, opacity .5s, visibility .5s;
    z-index: 1000;
    opacity: 0;
    visibility: hidden;
}

#top-btn.show {
    opacity: 1;
    visibility: visible;
}

#top-btn:hover {
    cursor: pointer;
    background-color: #333;
}

#top-btn:active {
    background-color: #555;
}

#top-btn::after {
    content: "\f077";
    font-family: FontAwesome;
    font-weight: normal;
    font-style: normal;
    font-size: 2em;
    line-height: 50px;
    color: #fff;
}
</style>

<div id="footer">
    <div class="footer-top">
        <div class="row">
            <div class="info col-md-4 mb-5">
                <h3>Freedom Developer&trade;</h3>
                <p>Email - <a href="mailto:info@freedeveloper.it">info@freedeveloper.it</p>
                </div>

                <div class="widget col-md-4 mb-5">
                    <div class="social-icon">
                        <h4>Seguici su</h4>
                        <a href="#"><i class="fa fa-facebook fa-lg"></i></a>
                        <a href="#"><i class="fa fa-twitter fa-lg"></i></a>
                        <a href="#"><i class="fa fa-linkedin fa-lg"></i></a>
                    </div>
                </div>

                <div class="credits col-md-4 mb-5">
                    <div class="row">
                        <p>Sviluppato da <strong>Tufano Raffaele</strong> & <strong>Genitoni Denis</strong></p>
                    </div>

                    <div class="row">

                    </div>
                </div>
            </div>
        </div>

        <div class="footer-bottom">
            <div class="copyright">
                <p class="text-center">Copyright &copy; Freedom Developer&trade; <?php echo date('Y'); ?>.</p>
            </div>
        </div>
        <a id="top-btn"></a>
    </div>
