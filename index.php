<!DOCTYPE html>
<html lang="it">
<head>
  <meta charset="utf-8">
  <title>Free Developer</title>
  <!-- Normalize CSS -->
  <link rel="stylesheet" href="assets/css/normalize.css">
  <!-- Google Fonts CSS -->
  <link href="https://fonts.googleapis.com/css?family=Share+Tech" rel="stylesheet">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
  <!-- FontAwesome CSS -->
  <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
  <!-- Custom CSS -->
  <link rel="stylesheet" href="assets/css/style.css">
</head>

<body data-spy="scroll" data-target="#navbar" data-offset="0">
  <!-- Navbar -->
  <?php include('functions/navbar.php'); ?>

  <!-- Header -->
  <?php include('functions/header.php'); ?>

  <!-- About -->
  <?php include('functions/about.php'); ?>

  <!-- Skills -->
  <?php include('functions/skills.php'); ?>

  <!-- our websites -->
  <?php include('functions/websites.php'); ?>

  <!-- Projects -->
  <?php include('functions/projects.php'); ?>

  <!-- Contact -->
  <?php 
    if(isset($_POST['send_contact'])){
       include('functions/email.php');
       $c_form = new contactForm();
       $return = $c_form->send_contact();
    }
  ?>
  <?php include('functions/contact.php'); ?>

  <!-- Footer -->
  <?php include('functions/footer.php'); ?>

  <!-- Modals -->
  <?php include('functions/modals.php'); ?>

  <!-- Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
  <!-- Custom JS -->
  <script src="assets/js/main.js"></script>
</body>
</html>
