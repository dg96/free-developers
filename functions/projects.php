<style type="text/css">
.projects{
  background-image: url('../img/jumbotron.jpg');
  background-attachment: fixed;
  background-size: cover;
  background-position: center;
  width: 100%;
  padding: 40px;
}

.projects .card{
  background-color: rgba(0,0,0,0.6);
  color: #ffffff;
}

.project .card h5{
  color: #3a84fc;
}
</style>

<div class="main-title" id="section-4">
  <h1>I nostri progetti</h1>
</div>

<div class="projects">
  <div class="row">
    <div class="card-deck">
      <div class="card">
        <img class="card-img-top" src="img/our-skill.jpg" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">SharekFile</h5>
          <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
          <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
        </div>
      </div>
      <div class="card">
        <img class="card-img-top" src="img/our-skill.jpg" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">Draft</h5>
          <p class="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
          <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
        </div>
      </div>
    </div>
  </div>
</div>
