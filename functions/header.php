<style type="text/css">
.jumbotron{
	background-image: url('../img/jumbotron.jpg');
	background-attachment: fixed;
	background-size: cover;
	background-position: center;
	color: #ffffff;
	height: 1000px;
	padding: 40px;
	margin: 0;
}
</style>

<div class="jumbotron jumbotron-fluid">
	<div class="container text-center my-5">
		<h1 class="display-4">Free Developer&trade;</h1>
		<p class="lead text-left">
			Hai un'idea? Ma non sai come trasformarla in realtà...
			Hai difficoltà nel risolvere un determinato problema?
			Ma non sai come risolvere il prima possibile...
			Benvenuto, sei proprio nel posto giusto!
			Visita il nostro sito per conoscerci e poi
			se sei sicuro anche tu di essere capitato nel posto
			giusto, non esitare...Contattaci!
		</p>
		<a href="#section-4">
			<button type="button" class="btn btn-outline-primary btn-lg">Contattaci</button>
		</a>
	</div>
</div>
