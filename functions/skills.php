<style type="text/css">
.skills {
  background-image: url('../img/jumbotron.jpg');
  background-attachment: fixed;
  background-size: cover;
  background-position: center;
  width: 100%;
  padding: 40px;
}

.skills .card-footer{background-color: black;}

.skills .card{color: white; background-color: #3f42ff; border: none;}

.skills .card-title{cursor: pointer;}
.skills .card-title:hover{color: black;}
.skills .card-title:focus{color: black;}

.skills .card-img-top {
  height: 250px;
}
</style>

<div class="main-title" id="section-2">
  <h1>Le nostre competenze</h1>
</div>

<div class="skills">
  <div class="row">
    <div class="card-deck">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Assistenza Tecnica</h3>
        </div>
        <div class="card-body">
          <img class="card-img-top" src="img/support.jpg" alt="Card image cap">
          <p class="card-text">
            Forniamo assistenza tecnica in ambito informatico,
            con varie soluzioni sia lato Hardware che Software.
          </p>
        </div>
        <div class="card-footer text-center">
          <button type="button" class="btn btn-outline-secondary">
            Richiedi preventivo
          </button>
        </div>
      </div>

      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Sviluppo Web</h3>
        </div>
        <div class="card-body">
          <img class="card-img-top" src="img/webdev.jpg" alt="Card image cap">
          <p class="card-text">
            Realizziamo progetti web su grande scala,
            dai siti web ad applicazioni complesse,
            come piattaforme cloud o gestionali.
          </p>
        </div>
        <div class="card-footer text-center">
          <button type="button" class="btn btn-outline-secondary">
            Richiedi preventivo
          </button>
        </div>
      </div>

      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Sviluppo Mobile</h3>
        </div>
        <div class="card-body">
          <img class="card-img-top" src="img/mobdev.jpg" alt="Card image cap">
          <p class="card-text">
            Sviluppiamo applicazioni mobile di qualsiasi tipo,
            per qualsiasi esigenza, e soprattutto per qualsiasi
            piattaforma.
          </p>
        </div>
        <div class="card-footer text-center">
          <button type="button" class="btn btn-outline-secondary">
            Richiedi preventivo
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
