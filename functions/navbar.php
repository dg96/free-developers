<style type="text/css">
.navbar {
    background-image: url('../img/jumbotron.jpg');
    background-attachment: fixed;
    background-size: cover;
    background-position: center;
}
</style>

<nav class="navbar navbar-expand-lg navbar-dark fixed-top">
    <a class="navbar-brand" href="#">Free Developer</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbar">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="#section-1">CHI SIAMO</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#section-2">COMPETENZE</a>
            </li>
            <li class="nav-item">
                <a href="#section-3" class="nav-link"> I NOSTRI SITI </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#section-4">PROGETTI</a>
            </li>
        </ul>
    </div>
</nav>
