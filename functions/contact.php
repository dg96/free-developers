<style type="text/css">
.contact{
  background-image:  linear-gradient(rgba(0,0,0,0.7), rgba(0,0,0,0.7)), url('../img/jumbotron.jpg');
  background-attachment: fixed;
  background-size: cover;
  background-position: center;
  color: #FFFFFF;
  width: 100%;
  padding: 40px;
}

.contact input{background-color: transparent; color: white; border: none;}
.contact input:hover{border: 1px solid #ffffff;}
.contact input:focus{background-color: transparent; border: 1px solid #ffffff;}
.contact textarea{background-color: transparent; color: white; border: none;}
.contact textarea:hover{border: 1px solid #ffffff;}
.contact textarea:focus{background-color: transparent; border: 1px solid #ffffff;}
</style>

<div class="main-title" id="section-5">
  <h1>Contattaci</h1>
</div>

<div class="contact">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-md-6 m-auto">
        <form action="#section-4" method="post">
          <?php
              global $return;
              if($return == true){
                  ?>
                    <div class="alert alert-success">
                        La tua richiesta è stata inviata correttamente. Grazie per averci contattato
                    </div>
                  <?php
              }
      ?>
          <div class="form-group">
            <input type="text" class="form-control" id="name" name="name" placeholder="Nome" required>
          </div>
          <div class="form-group">
            <input type="text" class="form-control" id="surname" name="surname" placeholder="Cognome" required>
          </div>
          <div class="form-group">
            <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
          </div>
          <div class="form-group">
            <input type="text" name="phone" id="phone" class="form-control" placeholder="Numero di telefono">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" id="subject" name="subject" placeholder="Oggetto" required>
          </div>
          <div class="form-group">
            <textarea class="form-control" id="message" name="message" rows="8" cols="80" placeholder="Messaggio" required></textarea>
          </div>
          <button type="submit" class="btn btn-outline-primary btn-block" name="send_contact">Invia</button>
        </form>
      </div>
    </div>
  </div>
</div>
