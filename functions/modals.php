<style type="text/css">
  .modal .modal-content{
    background-color: rgba(0,0,0,0.5);
    color: white;
  }
</style>



<div class="modal fade" id="denis-genitoni" tabindex="-1" role="dialog" aria-labelledby="denis-genitoniTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="denis-genitoniTitle">Denis Genitoni</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="raffaele-tufano" tabindex="-1" role="dialog" aria-labelledby="raffaele-tufanoTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="raffaele-tufanoTitle">Raffaele Tufano</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
    </div>
  </div>
</div>