<?php
	function send_email($address, $object, $text){
		$mittente = "info@freedeveloper.it";
		$name_mittente = "Free Developers";
		$mail_headers = "From: " . $name_mittente . " <" . $mittente . ">\r\n";
		$mail_headers .= "Reply-To: " . $mittente . "\r\n";
		$mail_headers .= "X-Mailer: PHP/" . phpversion();
		$mail_headers .= "MIME-Version: 1.0\n";
		$mail_headers .= "Content-Type: text/html; charset=\"utf-8\"\n";
		$mail_headers .= "Content-Transfer-Encoding: 7bit\n\n";

		if(mail($address, $object, $text, $mail_headers))
			return true;
		else
			return false;
	}

	class contactForm{
		public $name;
		public $surname;
		public $email;
		public $phone;
		public $object;
		public $text;

		public function __construct(){
			if(isset($_POST{'send_contact'})){
				$this->name = strip_tags($_POST{'name'});
				$this->surname = strip_tags($_POST{'surname'});
				$this->email = strip_tags($_POST{'email'});
				$this->phone = strip_tags($_POST['phone']);
				if($this->phone == "") $this->phone = "Numero di telefono non inserito";
				$this->object = strip_tags($_POST{'subject'});
				$this->text = strip_tags($_POST{'message'});
			}
		}

		public function send_contact(){
			$email_invio = "info@FreeDeveloper.it";
			$testo = "
			<p>
			<b> Questo messaggio è stato inviato dal signor: " . $this->name . " "  . $this->surname . " dal concact form di <a href='freedeveloper.it'> FreeDeveloper </a> </p>
			<p>
			<b> Testo del messaggio: </b> </p>
			<p>
			" . $this->text . "</p>
			<p>
			<b> Informazioni aggiuntive: </b> </p>
			<p> <b> Indirizzo email: </b> <i> " . $this->email . " </i> <br />
			<b> Numero di telefono: </b> <i> " . $this->phone . "</i>
			</p>";

			$oggetto = $this->object . " Richiesta freedeveloper.it";

			$result = send_email($email_invio, $oggetto, $testo);

			if($result == true){
				// invio messaggio al cliente
				$testo = "
				<p>
				Abbiamo ricevuto la tua richiesta di contatto. Ti risponderemo il prima possibile </p>
				<p>
				La copia del tuo messaggio: 
				<p>" . $this->text . "</p>";
				$oggetto = "Richiesta ricevuta - freedeveloper";

				$result = send_email($this->email, $oggetto, $testo);

				return $result;
			}
			else{
				return false;
			}
		}
	}
?>



