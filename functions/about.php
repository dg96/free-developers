<style type="text/css">
.about{
	background-image: url('../img/jumbotron.jpg');
  background-attachment: fixed;
  background-size: cover;
  background-position: center;
  width: 100%;
  padding: 40px;
}

.about .card-footer{
	background-color: #000000;
}

.about .card{
	width: auto;
	height: auto;
	border: 1px solid #000000;
	background-color: #3f42ff;
	color: white;
}

.about .card-title{cursor: pointer;}
.about .card-title:hover{color: black;}
.about .card-title:focus{color: black;}

.about .row .col-lg-12{
	height: auto;
	margin: 0;
	padding: 0;
}
</style>

<div class="main-title" id="section-1">
	<h1>Chi siamo</h1>
</div>

<div class="about">
	<div class="row">
		<div class="col-lg-4 mx-auto">
			<div class="card">
				<img class="card-img-top rounded-cicle" src="img/user-freedev.png" alt="Card image cap">
				<div class="card-body">
					<h5 class="card-title text-center">Denis Genitoni</h5>
					<p class="card-text text-center">Tecnico informatico specializzato.</p>
				</div>
				<div class="card-footer text-center">
					<button class="btn btn-outline-secondary btn-lg" data-toggle="modal" data-target="#denis-genitoni">Scopri di più...</button>
				</div>
			</div>
		</div>

		<div class="col-lg-4 mx-auto">
			<div class="card">
				<img class="card-img-top" src="img/user-freedev.png" alt="Card image cap">
				<div class="card-body">
					<h5 class="card-title text-center">Raffaele Tufano</h5>
					<p class="card-text text-center">Tecnico informatico specializzato.</p>
				</div>
				<div class="card-footer text-center">
					<button class="btn btn-outline-secondary btn-lg" data-toggle="modal" data-target="#raffaele-tufano">Scopri di più</button>
				</div>
			</div>
		</div>
	</div>
</div>
